package net.sf.okapi.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * User: Christian Hargraves
 * Date: Aug 13, 2009
 * Time: 8:08:16 AM
 */
@RunWith(JUnit4.class)
public class TestUtilTest {

    @Test
    public void getParentDir_FileNotFound() {
        assertNull("A parent directory for a nonexistent file should be null",
                TestUtil.getParentDir(this.getClass(), "some/nonexistent/file/that/could/no/way/exist.txt"));
    }

    @Test
    public void getParentDir_ValidFile() {
        String parentDir = TestUtil.getParentDir(this.getClass(), "/TestUtilTestTestFile.txt");
        assertNotNull(parentDir);
        assertTrue("Incorrect path returned", parentDir.endsWith("test-classes/"));
    }

    @Test
    public void checkXmlIgnoreAttributeOrder() {
		TestUtil.assertEquivalentXml("<ph type=\"deleted\" ID=\"1\">test content</ph>", "<ph ID=\"1\" type=\"deleted\">test content</ph>");
	}

    @Test
    public void testNormalize() throws Exception {
        String result = TestUtil
                .normalize("<root><tag b=\"foo\" c=\"bar\" a=\"zap\">content</tag></root>");
        assertEquals("<root><tag a=\"zap\" b=\"foo\" c=\"bar\">content</tag></root>", result);
    }

    @Test
    public void testNormalizeWithXmlDeclaration() throws Exception {
        String result = TestUtil
                .normalize("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "<root><tag b=\"foo\" c=\"bar\" a=\"zap\">content</tag></root>");
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<root><tag a=\"zap\" b=\"foo\" c=\"bar\">content</tag></root>", result);
    }

    @Test
    public void testNormalizeWithNamespacePrefix() throws Exception {
        String input =
                "<Xini "
                        + "SchemaVersion=\"1.0\" "
                        + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
                        + "xsi:noNamespaceSchemaLocation=\"http://www.ontram.com/xsd/xini.xsd\">"
                        + "<Main><Page PageID=\"1\">"
                        + "<PageName>xiniPack/original/test1.xlf</PageName>"
                        + "</Page></Main></Xini>";
        String result = TestUtil.normalize(input);
        assertEquals(input, result);
    }
}
