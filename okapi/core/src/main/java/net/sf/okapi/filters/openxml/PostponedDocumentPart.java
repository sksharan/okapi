package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.ISkeleton;
import net.sf.okapi.common.resource.Custom;

import java.util.zip.ZipEntry;

class PostponedDocumentPart extends Custom {
    private ZipEntry zipEntry;
    private boolean isPartHidden;
    private ISkeleton skeleton;

    PostponedDocumentPart(ISkeleton skeleton, ZipEntry zipEntry, boolean isPartHidden) {
        this.skeleton = skeleton;
        this.zipEntry = zipEntry;
        this.isPartHidden = isPartHidden;
    }

    ZipEntry getZipEntry() {
        return zipEntry;
    }

    boolean isPartHidden() {
        return isPartHidden;
    }

    @Override
    public ISkeleton getSkeleton() {
        return skeleton;
    }

    @Override
    public void setSkeleton(ISkeleton skeleton) {
        this.skeleton = skeleton;
    }
}
