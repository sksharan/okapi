/*===========================================================================
 Copyright (C) 2010-2013 by the Okapi Framework contributors
 -----------------------------------------------------------------------------
 This library is free software; you can redistribute it and/or modify it 
 under the terms of the GNU Lesser General Public License as published by 
 the Free Software Foundation; either version 2.1 of the License, or (at 
 your option) any later version.

 This library is distributed in the hope that it will be useful, but 
 WITHOUT ANY WARRANTY; without even the implied warranty of 
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License 
 along with this library; if not, write to the Free Software Foundation, 
 Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
 ===========================================================================*/

package net.sf.okapi.steps.characterschecker;

import net.sf.okapi.common.StringParameters;

public class Parameters extends StringParameters {

	private static final String CHECKCHARACTERS = "checkCharacters";
	private static final String CHECKALLOWEDCHARACTERS = "checkAllowedCharacters";
	private static final String CHARSET = "charset";
	private static final String EXTRACHARSALLOWED = "extraCharsAllowed";
	private static final String CORRUPTEDCHARACTERS = "corruptedCharacters";

	public Parameters() {
		super();
	}

	public boolean getCorruptedCharacters() {
		return getBoolean(CORRUPTEDCHARACTERS);
	}

	public void setCorruptedCharacters(boolean corruptedCharacters) {
		setBoolean(CORRUPTEDCHARACTERS, corruptedCharacters);
	}

	public boolean getCheckAllowedCharacters() {
		return getBoolean(CHECKALLOWEDCHARACTERS);
	}

	public void setCheckAllowedCharacters(boolean checkAllowedCharacters) {
		setBoolean(CHECKALLOWEDCHARACTERS, checkAllowedCharacters);
	}

	public boolean getCheckCharacters() {
		return getBoolean(CHECKCHARACTERS);
	}

	public void setCheckCharacters(boolean checkCharacters) {
		setBoolean(CHECKCHARACTERS, checkCharacters);
	}

	public String getCharset() {
		return getString(CHARSET);
	}

	public void setCharset(String charset) {
		setString(CHARSET, charset);
	}

	public String getExtraCharsAllowed() {
		return getString(EXTRACHARSALLOWED);
	}

	public void setExtraCharsAllowed(String extraCharsAllowed) {
		setString(EXTRACHARSALLOWED, extraCharsAllowed);
	}

	@Override
	public void reset() {
		super.reset();
		setCorruptedCharacters(true);
		setCheckAllowedCharacters(true);
		setCheckCharacters(false);
		setCharset("ISO-8859-1");
		setExtraCharsAllowed("");
	}

	@Override
	public void fromString(String data) {
		super.fromString(data);
	}

	@Override
	public String toString() {
		return super.toString();
	}
}
