/*===========================================================================
  Copyright (C) 2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.moses;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.CheckboxPart;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;

@EditorFor(MergingParameters.class)
public class MergingParameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String COPYTOTARGET = "copyToTarget";
	private static final String OVERWRITEEXISINGTARGET = "overwriteExistingTarget";
	private static final String FORCEALTTRANSOUTPUT = "forceAltTransOutput";
	private static final String USEGMODEINALTTRANS = "useGModeInAltTrans";
	
	public MergingParameters () {
		super();
	}
	
	public boolean getCopyToTarget () {
		return getBoolean(COPYTOTARGET);
	}

	public void setCopyToTarget (boolean copyToTarget) {
		setBoolean(COPYTOTARGET, copyToTarget);
	}

	public boolean getOverwriteExistingTarget () {
		return getBoolean(OVERWRITEEXISINGTARGET);
	}

	public void setOverwriteExistingTarget (boolean overwriteExistingTarget) {
		setBoolean(OVERWRITEEXISINGTARGET, overwriteExistingTarget);
	}

	public boolean getForceAltTransOutput () {
		return getBoolean(FORCEALTTRANSOUTPUT);
	}

	public void setForceAltTransOutput (boolean forceAltTransOutput) {
		setBoolean(FORCEALTTRANSOUTPUT, forceAltTransOutput);
	}

	public boolean getUseGModeInAltTrans () {
		return getBoolean(USEGMODEINALTTRANS);
	}

	public void setUseGModeInAltTrans (boolean useGModeInAltTrans) {
		setBoolean(USEGMODEINALTTRANS, useGModeInAltTrans);
	}

	@Override
	public void reset() {
		super.reset();
		setCopyToTarget(false);
		setOverwriteExistingTarget(false);
		setForceAltTransOutput(true);
		setUseGModeInAltTrans(true);
	}

	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(COPYTOTARGET, "Copy the leveraged translation into the target", null);
		desc.add(OVERWRITEEXISINGTARGET, "Overwrite any existing target text", null);
		desc.add(FORCEALTTRANSOUTPUT, "In XLIFF, force the new <alt-trans> in the output", null);
		desc.add(USEGMODEINALTTRANS, "Use the <g> notation in new <alt-trans> elements", null);
		return desc;
	}

	public EditorDescription createEditorDescription (ParametersDescription paramDesc) {
		EditorDescription desc = new EditorDescription(MergingStep.NAME, true, false);

		CheckboxPart cbp1 = desc.addCheckboxPart(paramDesc.get(COPYTOTARGET));
		CheckboxPart cbp2 = desc.addCheckboxPart(paramDesc.get(OVERWRITEEXISINGTARGET));
		cbp2.setMasterPart(cbp1, true);
		
		cbp1 = desc.addCheckboxPart(paramDesc.get(FORCEALTTRANSOUTPUT));
		desc.addCheckboxPart(paramDesc.get(USEGMODEINALTTRANS)).setMasterPart(cbp1, true);
		
		return desc;
	}

}
