/*===========================================================================
  Copyright (C) 2015 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.wordcount;

import static org.junit.Assert.assertEquals;
import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.ISegments;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TestCharacterCountStep {

	@Test
	public void testTextUnitCounts() {
		ITextUnit tu = new TextUnit("tu");
		TextContainer tc = tu.getSource();
		ISegments segments = tc.getSegments();
		segments.append(new TextFragment("The number of characters in this segment is 38."));
		segments.append(new TextFragment("The number of characters in this second segment is 44."));
		segments.append(new TextFragment("And the number of characters in this third segment is 46."));
		
		CharacterCountStep step = new CharacterCountStep();
		StartDocument sd = new StartDocument("sd");
		sd.setLocale(LocaleId.ENGLISH);
		step.handleEvent(new Event(EventType.START_DOCUMENT, sd));
		step.handleEvent(new Event(EventType.TEXT_UNIT, tu));
		
		assertEquals(38, CharacterCounter.getCount(tu, 0));
		assertEquals(44, CharacterCounter.getCount(tu, 1));
		assertEquals(46, CharacterCounter.getCount(tu, 2));
		assertEquals(128, CharacterCounter.getCount(tu));
	}
}
