/*===========================================================================
  Copyright (C) 2015 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.xliffsplitter;

import java.net.URI;
import java.security.InvalidParameterException;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.annotations.StepParameterMapping;
import net.sf.okapi.common.pipeline.annotations.StepParameterType;
import net.sf.okapi.common.resource.RawDocument;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@UsingParameters(XliffWCSplitterParameters.class)
public class XliffWCSplitterStep extends BasePipelineStep {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private XliffWCSplitterParameters params;
	
	public XliffWCSplitterStep() {
		params = new XliffWCSplitterParameters();
	}

//	@StepParameterMapping(parameterType = StepParameterType.OUTPUT_URI)
//	public void setOutputURI(final URI outputURI) {
//		this.outputURI = outputURI;
//	}
//	
//	public URI getOutputURI() {
//		return outputURI;
//	}
	
	@Override
	public String getDescription() {
		return "Split an XLIFF document into separate documents based on word count."
			+"Expects: raw document. Sends back: raw document.";
	}

	@Override
	public String getName() {
		return "XLIFF Word-Count Splitter";
	}

	@Override
	public IParameters getParameters() {
		return params;
	}

	@Override
	public void setParameters(final IParameters params) {
		this.params = (XliffWCSplitterParameters) params;
	}

	@Override
	protected Event handleRawDocument(final Event event) {
		final RawDocument rawDoc = event.getRawDocument();
		XliffWCSplitter splitter = new XliffWCSplitter(params);
		splitter.process(rawDoc);
		return event;
	}

}
