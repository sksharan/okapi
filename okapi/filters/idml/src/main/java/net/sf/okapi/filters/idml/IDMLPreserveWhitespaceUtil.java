/*
 * $Id$
 */
package net.sf.okapi.filters.idml;

import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;

/**
 * Utility class for adding the preserve whitespace flag to a ITextUnit object based on its
 * content string.
 *
 * @author Christian Spiess (cspiess)
 * @version $Rev$ $Date$
 * @since 12/1/15
 */
public class IDMLPreserveWhitespaceUtil {

    /**
     * Unicode Standard for white spaces
     * Translation to TagType.PLACEHOLDER in {@link IDMLFilter#processText(TextFragment, String)}
     */
    public static final String WHITESPACE_CHARS = ""/* dummy empty string for homogeneity */
            + "\\u0009" // CHARACTER TABULATION
            + "\\u000A" // LINE FEED (LF)
            + "\\u000B" // LINE TABULATION
            + "\\u000C" // FORM FEED (FF)
            + "\\u000D" // CARRIAGE RETURN (CR)
            + "\\u0020" // SPACE
            + "\\u0085" // NEXT LINE (NEL)
            + "\\u00A0" // NO-BREAK SPACE
            + "\\u1680" // OGHAM SPACE MARK
            + "\\u180E" // MONGOLIAN VOWEL SEPARATOR
            + "\\u2000" // EN QUAD
            //+ "\\u2001" // EM QUAD ==> will be translated to TagType.PLACEHOLDER, "sp-flush"
            + "\\u2002" // EN SPACE
            + "\\u2003" // EM SPACE
            //+ "\\u2004" // THREE-PER-EM SPACE ==> will be translated to TagType.PLACEHOLDER, "sp-3rd"
            //+ "\\u2005" // FOUR-PER-EM SPACE ==> will be translated to TagType.PLACEHOLDER, "sp-4th"
            //+ "\\u2006" // SIX-PER-EM SPACE ==> will be translated to TagType.PLACEHOLDER, "sp-6th"
            //+ "\\u2007" // FIGURE SPACE ==> will be translated to TagType.PLACEHOLDER, "sp-fig"
            //+ "\\u2008" // PUNCTUATION SPACE ==> will be translated to TagType.PLACEHOLDER, "sp-punc"
            //+ "\\u2009" // THIN SPACE ==> will be translated to TagType.PLACEHOLDER, "sp-thin"
            //+ "\\u200A" // HAIR SPACE ==> will be translated to TagType.PLACEHOLDER, "sp-hair"
            //+ "\\u2028" // LINE SEPARATOR ==> will be translated to TagType.PLACEHOLDER, "lb"
            + "\\u2029" // PARAGRAPH SEPARATOR
            //+ "\\u202F" // NARROW NO-BREAK SPACE ==> will be translated to TagType.PLACEHOLDER, "nbsp-fw"
            + "\\u205F" // MEDIUM MATHEMATICAL SPACE
            + "\\u3000" // IDEOGRAPHIC SPACE
            ;

    /**
     * updates the preserve whitespace flag of the given TU based on the white spaces of its content
     *
     * @param tu the ITextUnit object
     */
    public static void updateTu(ITextUnit tu) {
        if (tu == null) {
            return;
        }
        TextContainer source = tu.getSource();
        if (source == null) {
            return;
        }
        TextFragment firstContent = source.getFirstContent();
        if (firstContent == null) {
            return;
        }
        boolean hasWhitespaces = firstContent.getText().matches(".*[" + WHITESPACE_CHARS + "].*");
        tu.setPreserveWhitespaces(tu.preserveWhitespaces() || hasWhitespaces);
    }
}