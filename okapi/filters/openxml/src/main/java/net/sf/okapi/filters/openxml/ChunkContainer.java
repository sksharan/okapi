package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLStreamException;

interface ChunkContainer {
    void addChunk(Block.BlockChunk chunk) throws XMLStreamException;
}
