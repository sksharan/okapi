package net.sf.okapi.filters.openxml;

/**
 * Provides a mergeable run property interface.
 */
public interface MergeableRunProperty {

    /**
     * Checks whether a run property can be merged with another.
     *
     * @param runProperty A run property to merge with
     *
     * @return {@code true} if a property can be merged
     *         {@code false} otherwise
     */
    boolean canBeMerged(MergeableRunProperty runProperty);

    /**
     * Merges a run property with another.
     *
     * @param runProperty A run property to merge with
     *
     * @return A merged run property
     */
    MergeableRunProperty merge(MergeableRunProperty runProperty);
}
