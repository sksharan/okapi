package net.sf.okapi.filters.openxml;


import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;

import javax.xml.stream.XMLEventFactory;

public class StringItemTextUnitWriter implements TextUnitWriter{
    private final XMLEventFactory eventFactory;
    private final StringItem stringItem;
    private final XMLEventSerializer xmlWriter;
    private StringBuilder textContent = new StringBuilder();

    public StringItemTextUnitWriter(XMLEventFactory eventFactory, StringItem stringItem, XMLEventSerializer xmlWriter) {
        this.eventFactory = eventFactory;
        this.stringItem = stringItem;
        this.xmlWriter = xmlWriter;
    }

    public void write(TextContainer tc) {
        StyledText styledText = ((StyledText) stringItem.getChunks().get(1));
        xmlWriter.add(styledText.startElement);
        for (Segment segment : tc.getSegments()) {
            writeSegment(segment);
        }
        xmlWriter.add(eventFactory.createCharacters(textContent.toString()));
        xmlWriter.add(styledText.endElement);
    }

    private void writeSegment(Segment segment) {
        TextFragment content = segment.getContent();
        String codedText = content.getCodedText();
        for (int i = 0; i < codedText.length(); i++) {
            char c = codedText.charAt(i);
            writeChar(c);
        }
    }

    private void writeChar(char c) {
        textContent.append(c);
    }
}
