package net.sf.okapi.filters.openxml;

import java.util.Collections;

import static net.sf.okapi.filters.openxml.ClarifierStrategyFactoryValues.EMPTY_ATTRIBUTE_PREFIX;
import static net.sf.okapi.filters.openxml.ClarifierStrategyFactoryValues.RTL_BOOLEAN_VALUES;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_READING_ORDER;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_RIGHT_TO_LEFT;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_RTL;

/**
 * Provides a markup component clarifier strategy factory.
 */
class MarkupComponentClarifierStrategyFactory {

    private static final String READING_ORDER_RTL_VALUE = "2";

    /**
     * Creates a presentation markup component clarifier strategy.
     *
     * @param creationalParameters    Creational parameters
     * @param clarificationParameters Clarification parameters
     *
     * @return A presentation markup component clarifier strategy
     */
    static MarkupComponentClarifierStrategy createPresentationMarkupComponentClarifierStrategy(CreationalParameters creationalParameters, ClarificationParameters clarificationParameters) {
        return new MarkupComponentClarifierStrategy(creationalParameters, clarificationParameters,
                new ClarifiableAttribute(EMPTY_ATTRIBUTE_PREFIX, LOCAL_RTL, RTL_BOOLEAN_VALUES));
    }

    /**
     * Creates a sheet view markup component clarifier strategy.
     *
     * @param creationalParameters    Creational parameters
     * @param clarificationParameters Clarification parameters
     *
     * @return A sheet view markup component clarifier strategy
     */
    static MarkupComponentClarifierStrategy createSheetViewMarkupComponentClarifierStrategy(CreationalParameters creationalParameters, ClarificationParameters clarificationParameters) {
        return new MarkupComponentClarifierStrategy(creationalParameters, clarificationParameters,
                new ClarifiableAttribute(EMPTY_ATTRIBUTE_PREFIX, LOCAL_RIGHT_TO_LEFT, RTL_BOOLEAN_VALUES));
    }

    /**
     * Creates an alignment markup component clarifier strategy.
     *
     * @param creationalParameters    Creational parameters
     * @param clarificationParameters Clarification parameters
     *
     * @return An alignment markup component clarifier strategy
     */
    static MarkupComponentClarifierStrategy createAlignmentMarkupComponentClarifierStrategy(CreationalParameters creationalParameters, ClarificationParameters clarificationParameters) {
        return new MarkupComponentClarifierStrategy(creationalParameters, clarificationParameters,
                new ClarifiableAttribute(EMPTY_ATTRIBUTE_PREFIX, LOCAL_READING_ORDER, Collections.singletonList(READING_ORDER_RTL_VALUE)));
    }
}
