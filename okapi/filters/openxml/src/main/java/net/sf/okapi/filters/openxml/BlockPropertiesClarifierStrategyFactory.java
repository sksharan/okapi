package net.sf.okapi.filters.openxml;

import net.sf.okapi.filters.openxml.BlockPropertiesClarifierStrategy.PropertiesClarifierStrategy.ParagraphPropertiesClarifierStrategy;

import static net.sf.okapi.filters.openxml.ClarifierStrategyFactoryValues.EMPTY_ATTRIBUTE_PREFIX;
import static net.sf.okapi.filters.openxml.ClarifierStrategyFactoryValues.RTL_BOOLEAN_VALUES;
import static net.sf.okapi.filters.openxml.Namespaces.DrawingML;
import static net.sf.okapi.filters.openxml.Namespaces.fromNamespaceURI;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_RTL;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_RTL_COL;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.PREFIX_A;

/**
 * Provides a block properties clarifier strategy factory.
 */
class BlockPropertiesClarifierStrategyFactory {

    /**
     * Creates a paragraph properties clarifier strategy.
     *
     * @param creationalParameters    Creational parameters
     * @param clarificationParameters Clarification parameters
     *
     * @return A paragraph properties clarifier strategy
     */
    static BlockPropertiesClarifierStrategy createParagraphPropertiesClarifierStrategy(CreationalParameters creationalParameters, ClarificationParameters clarificationParameters) {
        return new ParagraphPropertiesClarifierStrategy(creationalParameters, clarificationParameters);
    }

    /**
     * Creates a text body properties clarifier strategy.
     *
     * @param creationalParameters    Creational parameters
     * @param clarificationParameters Clarification parameters
     *
     * @return A text body properties clarifier strategy
     */
    static BlockPropertiesClarifierStrategy createTextBodyPropertiesClarifierStrategy(CreationalParameters creationalParameters, ClarificationParameters clarificationParameters) {
        CreationalParameters newCreationalParameters = (DrawingML != fromNamespaceURI(creationalParameters.getNamespaceUri()))
                ? new CreationalParameters(
                    creationalParameters.getEventFactory(),
                    PREFIX_A,
                    DrawingML.getURI())
                : creationalParameters;

        return new BlockPropertiesClarifierStrategy.AttributesClarifierStrategy.TextBodyPropertiesClarifierStrategy(newCreationalParameters, clarificationParameters,
                new ClarifiableAttribute(EMPTY_ATTRIBUTE_PREFIX, LOCAL_RTL_COL, RTL_BOOLEAN_VALUES));
    }

    /**
     * Creates a table properties clarifier strategy.
     *
     * @param creationalParameters    Creational parameters
     * @param clarificationParameters Clarification parameters
     *
     * @return A table properties clarifier strategy
     */
    static BlockPropertiesClarifierStrategy createTablePropertiesClarifierStrategy(CreationalParameters creationalParameters, ClarificationParameters clarificationParameters) {
        if (DrawingML == fromNamespaceURI(creationalParameters.getNamespaceUri())) {
            return new BlockPropertiesClarifierStrategy.AttributesClarifierStrategy.TablePropertiesClarifierStrategy(creationalParameters, clarificationParameters,
                    new ClarifiableAttribute(EMPTY_ATTRIBUTE_PREFIX, LOCAL_RTL, RTL_BOOLEAN_VALUES));
        }

        return new BlockPropertiesClarifierStrategy.PropertiesClarifierStrategy.TablePropertiesClarifierStrategy(creationalParameters, clarificationParameters);
    }
}
