package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.IResource;
import net.sf.okapi.common.ISkeleton;

import java.util.Map;

class StringItemSkeleton implements ISkeleton {
    private StringItem stringItem;
    private IResource parent;
    private Map<Integer, XMLEvents> codeMap;

    public StringItemSkeleton(StringItem stringItem, Map<Integer, XMLEvents> codeMap) {
        this.stringItem = stringItem;
        this.codeMap = codeMap;
    }

    @Override
    public ISkeleton clone() {
        StringItemSkeleton stringItemSkeleton = new StringItemSkeleton(stringItem, codeMap);
        stringItemSkeleton.setParent(getParent());
        return stringItemSkeleton;
    }

    public StringItem getStringItem() {
        return stringItem;
    }

    @Override
    public void setParent(IResource parent) {
        this.parent = parent;
    }

    @Override
    public IResource getParent() {
        return parent;
    }
}
