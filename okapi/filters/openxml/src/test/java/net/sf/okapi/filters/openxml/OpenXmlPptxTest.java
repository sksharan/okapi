/*===========================================================================
  Copyright (C) 2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.openxml;

import static net.sf.okapi.filters.openxml.CodePeekTranslator.locENUS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.net.URL;
import java.util.ArrayList;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class OpenXmlPptxTest{

	@Test
	public void testMaster() throws Exception {
		ConditionalParameters params = new ConditionalParameters();
		params.setTranslateDocProperties(false);
		params.setTranslatePowerpointMasters(true);
		params.setIgnorePlaceholdersInPowerpointMasters(true);

		OpenXMLFilter filter = new OpenXMLFilter();
		filter.setParameters(params);

		URL url = getClass().getResource("/textbox-on-master.pptx");

		RawDocument doc = new RawDocument(url.toURI(),"UTF-8", locENUS);
		ArrayList<Event> events = getEvents(filter, doc);
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("<run1>My title</run1>", tu.getSource().toString());

		tu = FilterTestDriver.getTextUnit(events, 2);
		assertNotNull(tu);
		assertEquals("<run1>My subtitle</run1>", tu.getSource().toString());

		tu = FilterTestDriver.getTextUnit(events, 3);
		assertNotNull(tu);
		assertEquals("<run1>Textbox on layout 1</run1>", tu.getSource().toString());

		tu = FilterTestDriver.getTextUnit(events, 4);
		assertNotNull(tu);
		assertEquals("<run1>Textbox on master</run1>", tu.getSource().toString());

		tu = FilterTestDriver.getTextUnit(events, 5);
		assertNull(tu);
	}

	private ArrayList<Event> getEvents(OpenXMLFilter filter, RawDocument doc) {
		ArrayList<Event> list = new ArrayList<>();
		filter.open(doc, false);
		while (filter.hasNext()) {
			Event event = filter.next();
			list.add(event);
		}
		filter.close();
		return list;
	}
}
